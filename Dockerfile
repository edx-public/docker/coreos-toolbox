############################################################
# Dockerfile to build CoreOS Toolbox container images
# Based on Latest Kali Linux
############################################################

# Set the base image to kalilinux
FROM kalilinux/kali-linux-docker

# File Author / Maintainer
MAINTAINER Stéphane (Educatux) GAUTIER

ENV HOME /root

RUN apt-get -yqq update \
    && apt-get install -yqq zsh git tmux tree mc htop \
	vim wget curl sudo dnsutils ansible p7zip-full \
    && apt-get clean

################## BEGIN INSTALLATION ######################
RUN groupadd -g 500 core \
    && adduser --uid=500 --gid=500 --shell /bin/zsh --disabled-password --gecos 'CoreOS Admin' core

RUN sudo -u core git clone git://github.com/bwithem/oh-my-zsh.git /home/core/.oh-my-zsh \
    && sudo -u core cp /home/core/.oh-my-zsh/templates/zshrc.zsh-template /home/core/.zshrc

RUN wget -O /root/.zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc \
    && chsh -s /bin/zsh

RUN sed -i -E "s/^plugins=\((.*)\)$/plugins=(\1 tmux)/" /home/core/.zshrc
RUN echo "core ALL=NOPASSWD: ALL" > /etc/sudoers.d/core

WORKDIR /root

ENTRYPOINT ["/bin/zsh"]
